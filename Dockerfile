FROM registry.gitlab.com/gitlab-org/frontend/frontend-build-images/node-fips:v16.4.0

COPY . /test

RUN cd /test && yarn && node ./benchmark.js