// Code taken from https://medium.com/@chris_72272/what-is-the-fastest-node-js-hashing-algorithm-c15c1a0e164e

const Benchmark = require('benchmark');
const suite = new Benchmark.Suite();
const crypto = require('crypto');
const hash = crypto.createHash;

const data =
  'Delightful remarkably mr on announcing themselves entreaties favourable. About to in so terms voice at. Equal an would is found seems of. The particular friendship one sufficient terminated frequently themselves. It more shed went up is roof if loud case. Delay music in lived noise an. Beyond genius really enough passed is up.';

const unsupported = new Set();

for (const alg of crypto.getHashes()) {
  try {
    hash(alg).update(data).digest('hex');
    suite.add(`${alg}-hex`, () => hash(alg).update(data).digest('hex'));
  } catch (e) {
    unsupported.add(alg);
  }
}
suite
  .on('cycle', function (event) {
    console.log(String(event.target));
  })
  .on('complete', function () {
    console.log('Fastest is ' + this.filter('fastest').map('name'));
    if (unsupported.size > 0) {
      console.log('The following algorithms are unsupported: ');
      console.log(unsupported);
    }
  })
  .run();
